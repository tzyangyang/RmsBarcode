﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RmsBarcode
{
    interface ICode
    {
        string Encoded_Value
        {
            get;
        }//Encoded_Value

        string RawData
        {
            get;
        }//Raw_Data

        string FormattedData
        {
            get;
        }//FormattedData
    }
}
